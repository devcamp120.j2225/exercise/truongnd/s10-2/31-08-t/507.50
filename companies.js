class Companies {
    constructor(paramId, paramName, paramContact, paramCountry) {
      this.id = paramId;
      this.name = paramName;
      this.contact = paramContact;
      this.country = paramCountry;
    }
  }
  
  //Khởi tạo class
  let company_1 = new Companies(1, "Alfreds Futterkiste","Maria Anders", "Germany");
  let company_2 = new Companies(2, "Centro comercial Moctezuma","Francisco Chang", "Mexico");
  let company_3 = new Companies(3, "Ernst Handel","Roland Mendel", "Austria");
  let company_4 = new Companies(4, "Island Trading","Helen Bennett", "UK");
  let company_5 = new Companies(5, "Laughing Bacchus Winecellars","Yoshi Tannamuri", "Canada");
  let company_6 = new Companies(6, "Magazzini Alimentari Riuniti","Giovanni Rovelli", "Italy");
  
  let companiesList = [company_1, company_2, company_3, company_4, company_5, company_6];
  
  module.exports = {companiesList};