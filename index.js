const express = require('express');

const app = new express();

const port = 8000;

//import router modules
const companiesRouter = require("./app/routes/companyRouter");

//middleware chạy ra thời gian &  phương thức req
app.use((req,res,next)=> {
    console.log(new Date());
    next()
}, (req,res,next)=> {
    console.log(req.method);
    next()
})

// app.use((req,res,next)=> {
//     console.log(req.method);
//     next()
// })


app.get("/",(req,res) => {
    let today = new Date();
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`,
    })
})

app.use(companiesRouter)


app.listen(port,() => {
    console.log(`app listen on port ${port}`)
})
