const express = require("express");

const { companyMiddleware } = require('../middlewares/companyMiddleware');
const router = express.Router();
router.use(companyMiddleware);

const {companiesList} = require("../../companies")

router.get("/companies", (req, res) => {
    res.status(200).json({
        companies: companiesList,
    })
})

router.get("/companies/:companyId",(req,res) =>{
    let companyId = req.params.companyId ;
    res.status(200).json({
        message: `GET company BY ID : ` + companyId 
    })
})

router.post("/companies", (req, res) => {
    res.status(200).json({
        message: `CREATE company`
    })
})

router.put("/companies/:companyId",(req,res) =>{
    let companyId = req.params.companyId ;
    res.status(200).json({
        message: `UPDATE company WITH ID : ` + companyId 
    })
})

router.delete("/companies/:companyId",(req,res) =>{
    let companyId = req.params.companyId ;
    res.status(200).json({
        message: `DELETE company WITH ID : ` + companyId 
    })
})

module.exports = router;

